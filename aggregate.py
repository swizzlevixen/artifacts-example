# -*- coding: utf-8 -*-
import requests, zipfile
import io
import json
import os

from progress.bar import Bar


class CONFIG:
    PROJECT_ID = os.environ.get("PROJECT_ID")
    GITLAB_URL = "https://gitlab.com/api/v4/"
    API_KEY = os.environ.get("ACCESS_TOKEN")
    EXTRACT_DIR = "data/"


class NewsItem(object):
    def __init__(self, data) -> None:
        self.data = data
        self.id = int(data["id"])

    def __hash__(self) -> int:
        return hash(self.id)

    def __eq__(self, __o: object) -> bool:
        return isinstance(__o, NewsItem) and self.id == __o.id

    def __repr__(self) -> str:
        return f"NewsItem:{self.id}"


def get_finished_jobs() -> dict:
    url = f"{CONFIG.GITLAB_URL}projects/{CONFIG.PROJECT_ID}/jobs?scope[]=success"
    r = requests.get(url=url, headers={"PRIVATE_TOKEN": CONFIG.API_KEY})
    return json.loads(r.content)


def download_artifact(job_id, extract_dir):
    url = f"{CONFIG.GITLAB_URL}projects/{CONFIG.PROJECT_ID}/jobs/{job_id}/artifacts"
    r = requests.get(url=url, headers={"PRIVATE_TOKEN": CONFIG.API_KEY}, stream=True)
    z = zipfile.ZipFile(io.BytesIO(r.content))
    if not os.path.exists(extract_dir):
        os.makedirs(extract_dir)
    z.extractall(extract_dir)


def download_all_artifacts(extract_dir):
    jobs = get_finished_jobs()
    print(jobs)
    bar = Bar("Downloading Artifacts", max=len(jobs))
    for j in jobs:
        download_artifact(j["id"], extract_dir)
        bar.next()
    bar.finish()


def read_all_artifacts(extract_dir):
    all_news = set()
    for file in os.listdir(extract_dir):
        full_filename = f"{extract_dir}/{file}"
        with open(full_filename, "r") as fi:
            artifact_news = json.load(fi)
            for n in artifact_news["default"].values():
                all_news.add(NewsItem(n))
    return all_news


print(CONFIG.PROJECT_ID)
download_all_artifacts(CONFIG.EXTRACT_DIR)
print(len(read_all_artifacts(CONFIG.EXTRACT_DIR)))
