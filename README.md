# artifacts-example

A learning project for persisting data across CI runs using artifacts.

## Usage
Check the blog post [Using Gitlab’s CI for Periodic Data Mining](https://towardsdatascience.com/using-gitlabs-ci-for-periodic-data-mining-b3cc314ecd85), but basically it needs to be set up with GitLab CI to create artifacts from the pipeline on a regular basis.

## Roadmap
- Get GitLab API access working — `aggregate.py` seems not to be authenticating correctly.
- I'd like to see if I can actually run `agregate.py` during the CI pipeline, pull from the API to get the last successful pipeline's artifact, and load that in as new data to compare against during the next scheduled run of the pipeline. Basically saving a tiny database to be used across all runs of the pipeline.

## Authors and acknowledgment
Project and modifications by Mark Boszko.

Based on [Using Gitlab’s CI for Periodic Data Mining](https://towardsdatascience.com/using-gitlabs-ci-for-periodic-data-mining-b3cc314ecd85) by Andreas Pogiatzis, who further evolved the code in [sigmalive-rss-feed-miner](https://github.com/apogiatzis/sigmalive-rss-feed-miner).

## License
MIT License.

## Project status
This is a test project and is not expected to be updated often.
