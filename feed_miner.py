# -*-coding: utf-8 -*-
import logging
import requests
import xmltodict
import time
import sys

from tinydb import TinyDB

RSS_FEED_URL = "https://beforesandafters.com/feed/"

logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger(__name__)


def fetch_news(*, db_path):
    logger.info("Fetching news from Befores and Afters...")

    rss_content = requests.get(RSS_FEED_URL).text
    parsed_feed = xmltodict.parse(rss_content)

    logger.info(
        f"Found {len(parsed_feed['rss']['channel']['item'])} items in RSS feed."
    )

    db = TinyDB(db_path)
    for item in parsed_feed["rss"]["channel"]["item"]:
        db.insert(item)

    logger.info(f"Stored {len(parsed_feed['rss']['channel']['item'])} items in DB.")


def get_stored_news(*, db_path):
    return TinyDB(db_path).all()


if __name__ == "__main__":
    fetch_news(db_path=f"news_{int(time.time())}.json")
